

import java.util.ArrayList;

public abstract class Utilisateur {

    protected String nom;
    protected String prenom;
    protected String nomUtilisateur;
    protected String mdp;
    protected Integer telephone;
    protected String mail;
    protected String bureau;
    public String role;
    public ArrayList info;

    public Utilisateur(String nom, String mdp, String role) {
        this.nomUtilisateur = nom;
        this.mdp = mdp;
        this.role = role;

    }
    //Constructeur complet

    public Utilisateur(String prenom, String nom, String nomUtilisateur, String mdp, String role) {
        this.prenom = prenom;
        this.nom = nom;
        this.nomUtilisateur = nomUtilisateur;
        this.mdp = mdp;
        this.role = "client";
    }

    public String getRole() {
        return role;
    }

    //donne la liste des infos de l'utilisateur
    public ArrayList<String> fetchInfos() {
        info.add(nom);
        info.add(prenom);
        info.add(nomUtilisateur);
        info.add(telephone.toString());
        info.add(mail);
        info.add(bureau);
        info.add(role);
        return info;
    }

    public String getNom() {
        return nomUtilisateur;
    }

    //Verifie les informations à la connexion
    public boolean login(String util, String motdp) {
        if (nomUtilisateur.equalsIgnoreCase(util) && mdp.equals(motdp)) {
            return true;
        } else {
            return false;
        }

    }

    public String getNomUtil() {
        return nomUtilisateur;
    }

    public abstract ArrayList<Requete> getListStatut(Requete.Statut statut);

    public abstract ArrayList<Requete> getListeRequetes();

    public abstract void ajoutRequete(Requete nouvelle);

    public abstract ArrayList<Requete> getListPerso();
}