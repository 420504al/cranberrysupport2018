

import java.util.ArrayList;

public class Client extends Utilisateur {

    private ArrayList<Requete> listeRequetesClient;

    //constructeur temporaire
    public Client(String nom, String mdp, String role) {
        super(nom, mdp, role);
        listeRequetesClient = new ArrayList<Requete>();

    }

    //Constructeur de client complet
    Client(String prenom, String nom, String nomUtilisateur, String mdp, String role) {
        super(prenom, nom, nomUtilisateur, mdp, role);
        listeRequetesClient = new ArrayList<Requete>();
    }

    @Override
    public String getRole() {
        return "client";
    }

    @Override
    public void ajoutRequete(Requete nouvelle) {
        listeRequetesClient.add(nouvelle);
    }

    public void trouverRequete() {
    }
    
    @Override
    public ArrayList getListeRequetes() {
        return listeRequetesClient;
    }

    @Override
    public ArrayList<Requete> getListPerso() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList<Requete> getListStatut(Requete.Statut statut) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}